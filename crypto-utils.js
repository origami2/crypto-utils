var assert = require('assert');
var RSA = require('node-rsa');

function asPrivateKey(privateKey) {
  if (typeof(privateKey) === 'string') {
    var key = new RSA();

    try {
      key.importKey(privateKey, 'pkcs1-private-pem');
    } catch (e) {
      throw new Error('invalid private key');
    }

    return key;
  }

  return privateKey;
}

function asPublicKey(publicKey) {
  if (typeof(publicKey) === 'string') {
    var key = new RSA();

    try {
      key.importKey(publicKey, 'pkcs1-public-pem');
    } catch (e) {
      throw new Error('invalid public key');
    }

    return key;
  }

  return publicKey;
}

function randomKey(size) {
  var key = new RSA();
  
  key.generateKeyPair(size || 512);
  
  return key.exportKey('pkcs1-private-pem');  
}

function asString(key, exportPrivate) {
  if (!key) throw new Error('key is required');
  
  if (!exportPrivate) return key.exportKey('pkcs1-public-pem');
  else return key.exportKey('pkcs1-private-pem');
}

var utils = {
  randomKey: randomKey,
  asString: asString,
  asPublicKey: asPublicKey,
  asPrivateKey: asPrivateKey,
  encryptAndSign: function (input, signKey, encryptKey) {
    signKey = asPrivateKey(signKey);
    encryptKey = asPublicKey(encryptKey);

    return utils.sign(utils.encrypt(input, encryptKey), signKey);
  },
  decryptAndVerify: function (input, decryptKey, verifyKey) {
    verifyKey = asPublicKey(verifyKey);
    decryptKey = asPrivateKey(decryptKey);

    var verifies = utils.verify(input, verifyKey);

    assert.equal(true, verifies, 'fails to verify signature');

    return utils.decrypt(input.data, decryptKey);
  },
  encrypt: function (input, publicKey) {
    if (!input) throw new Error('input data is required');
    if (!publicKey) throw new Error('public key is required');

    publicKey = asPublicKey(publicKey);

    var normalInput = JSON.stringify(input);

    var buffer = publicKey.encrypt(normalInput);

    return buffer.toString('base64');
  },
  decrypt: function (input, privateKey) {
    if (!input) throw new Error('input data is required');
    if (!privateKey) throw new Error('private key is required');

    privateKey = asPrivateKey(privateKey);

    var decrypted = privateKey.decrypt(input);

    return JSON.parse(decrypted);
  },
  sign: function (input, privateKey) {
    if (!input) throw new Error('input data is required');
    if (!privateKey) throw new Error('private key is required');

    privateKey = asPrivateKey(privateKey);

    var normalInput = JSON.stringify(input);

    return {
      data: normalInput,
      signature: privateKey.sign(normalInput, 'base64')
    };
  },
  verify: function (input, publicKey) {
    if (!input) throw new Error('input data is required');
    if (!publicKey) throw new Error('public key is required');

    if (typeof(input.data) !== 'string' ||
        typeof(input.signature) !== 'string') {
      throw new Error('invalid input data');
    }

    publicKey = asPublicKey(publicKey);

    try {
      return publicKey.verify(input.data, input.signature, null, 'base64');
    } catch (e) {
      throw new Error('fails to verify signature');
    }
  }
};

module.exports = utils;
