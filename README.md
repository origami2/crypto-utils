# Origami crypto utils

### Purpose

Provided common utilitary methods for cryptographic functions:

- generate new key pairs
- encrypt data
- sign data
- decrypt data
- verify signatures
- encrypt and sign in the same step
- decrypt and sign in the same step

It uses PCKS1-PEM key formats.

### Usage

```javascript
var crypto = require('origami-crypto-utils');

var key1 = crypto.randomKey();
var key2 = crypto.randomKey();

var encrypted = crypto.encryptAndSign(
  {
    hello: 'there'
  },
  key1, // private key for signing,
  key2 // public key for encrypting
);

var decrypted = crypto.decryptAndVerify(
  encrypted,
  key2, // private key for decrypting
  key1 // public key for signature verification
);
```